<?php
	class ComputersController extends AppController{
		public $helpers = array('Html', 'Form');
		public $components = array('Session');
		
		public function index(){
			$this->set('computadoras', $this->Computer->find('all'));
		}
		
		public function add() {
			if($this->request->is('post')):
				if($this->Computer->save($this->request->data)):
					$this->Session->setFlash('Equipo guardado');
					$this->redirect(array('action' => 'index'));
				endif;
			endif;
		}
	
		public function edit($id = null){
			$this->Computer->id = $id;
			if($this->request->is('get')):
				$this->request->data = $this->Computer->read();
			else:
				if($this->Computer->save($this->request->data)):
					$this->Session->setFlash('Captura guardada');
					$this->redirect(array('action' => 'index'));
				else:
					$this->Session->setFlash('No se pudo guardar');
				endif;
			endif;
		}
	}
